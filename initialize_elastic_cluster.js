const elasticsearch = require('elasticsearch');
const ElastiClient = new elasticsearch.Client({
  host: 'http://104.197.118.37/elasticsearch',
  httpAuth: 'user:P4d61bXKw6fC'
});

/*const ElastiClient = new elasticsearch.Client({
 host: 'http://ns508027.ip-198-245-60.net:9200'
 });*/


ElastiClient.indices.get({'index': 'digging'}, function(e,r){
  if(r.digging)
  {
    mapReleaseType();
  }
  else
  {
    ElastiClient.indices.create({'index': 'digging'}, function(){
      mapReleaseType();
    });
  }
});



mapReleaseType = function(){
  var params = {};
  params.index = 'digging';
  params.type = 'release';
  params.body = {"release" : {
    "properties":{
      "alias":{
        "type":"string",
        "store":true
      },
      "artist":{
        "type":"string",
        "store":true
      },
      "cover":{
        "type":"string",
        "store":true
      },
      "date":{
        "type":"date",
        "store":true,
        "format":"strict_date_optional_time||epoch_millis"
      },
      "genre":{
        "type":"string"
      },
      "id":{
        "type":"string"
      },
      "item_type":{
        "type":"string"
      },
      "kind":{
        "type":"string",
        "store":true
      },
      "label":{
        "type":"string",
        "store":true
      },
      "logo":{
        "type":"string"
      },
      "played_by":{
        "type":"string",
        "store":true
      },
      "query":{
        "type":"string",
        "store":true
      },
      "release_date":{
        "type":"date",
        "store":true,
        "format":"strict_date_optional_time||epoch_millis"
      },
      "source":{
        "type":"string",
        "store":true
      },
      "tab_id":{
        "type":"string"
      },
      "title":{
        "type":"string",
        "store":true
      },
      "tms":{
        "type":"string",
        "store":true
      },
      "tracks":{
        "type":"nested",
        "include_in_parent":true,
        "properties":{
          "_id":{
            "type":"string",
            "store":true
          },
          "alias":{
            "type":"string",
            "store":true
          },
          "artist":{
            "type":"string",
            "store":true
          },
          "col":{
            "type":"long"
          },
          "cover":{
            "type":"string",
            "store":true
          },
          "duration":{
            "type":"string",
            "store":true
          },
          "genre":{
            "type":"string",
            "store":true
          },
          "id":{
            "type":"string",
            "store":true
          },
          "item_type":{
            "type":"string"
          },
          "kind":{
            "type":"string",
            "store":true
          },
          "label":{
            "type":"string",
            "store":true
          },
          "logo":{
            "type":"string",
            "store":true
          },
          "played_by":{
            "type":"string",
            "store":true
          },
          "query":{
            "type":"string",
            "store":true
          },
          "release_date":{
            "type":"date",
            "store":true,
            "format":"strict_date_optional_time||epoch_millis"
          },
          "release_title":{
            "type":"string"
          },
          "sample":{
            "type":"string",
            "store":true
          },
          "source":{
            "type":"string",
            "store":true
          },
          "tab_id":{
            "type":"string"
          },
          "title":{
            "type":"string",
            "store":true
          },
          "tms":{
            "type":"string",
            "store":true
          },
          "type":{
            "type":"string"
          },
          "url":{
            "type":"string"
          },
          "year":{
            "type":"string",
            "store":true
          },
          "youtube":{
            "type":"string",
            "store":true
          }
        }
      },
      "type":{
        "type":"string"
      },
      "url":{
        "type":"string",
        "store":true
      },
      "year":{
        "type":"string",
        "store":true
      }
    }
  }};
  ElastiClient.indices.putMapping(params, function(error, result){
    console.log(error);
    console.log(result);
  });
};


