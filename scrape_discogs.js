const discogs = require('./common/digging/sources/discogs');
const curl = require('./common/digging/curl');
const request = require('request');
const digging = require('./common/digging/digging');
const elasticsearch = require('elasticsearch');
const ElastiClient = new elasticsearch.Client({
  host: 'http://104.197.118.37/elasticsearch',
  httpAuth: 'user:P4d61bXKw6fC'
});

get_release = function(){
  var id = Math.floor(Math.random() * 10997794) + 1;
  var url = "https://www.discogs.com/xxxx/release/"+id;

  var sleeps = [];
  sleeps.push(900);
  sleeps.push(1800);
  sleeps.push(1900);
  sleeps.push(1200);
  sleeps.push(1600);
  sleeps.push(3600);
  sleeps.push(2900);
  sleeps.push(950);
  sleeps.push(1550);
  sleeps.push(2100);
  sleeps.push(1700);
  sleeps.push(1500);
  sleeps.push(1700);

  var user_agents = [];
  user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2");
  user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
  user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.0; it-IT; rv:1.7.12) Gecko/20050915");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.12 Safari/537.36 OPR/14.0.1116.4");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36 OPR/15.0.1147.24 (Edition Next)");
  user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
  user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
  user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
  user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");

  var random_index = Math.floor((Math.random() * 10));
  var user_agent = user_agents[random_index];

  var wait_time = user_agents[random_index];

  setTimeout(function(){
    request(url, {headers: {
      "User-Agent": user_agent,
      "timeout": 2000
    }}, function (error, response, html) {

      if(error)
      {
        get_release();
      }
      else
      {
        var r = discogs.parse_detail(html, url);

        if(r)
        {
          var release = digging.prepare_release({logo: "", name: "Discogs", value: "discogs"}, r);
          if(release)
          {
            if(release.id)
            {
              ElastiClient.search({
                index: 'digging',
                type: 'release',
                body: {
                  "query": {
                    "terms": {
                      "_id": [ release.id ]
                    }
                  }
                }
              },function (error, response, status) {

                if(!error && response.hits.total == 0)
                {
                  ElastiClient.index({
                    index: 'digging',
                    type: 'release',
                    id: release.id,
                    body: release
                  }, function (error, response) {
                    console.log('releases inserted');
                    get_release();
                  });
                }
                else
                {
                  get_release();
                }
              });
            }
          }
          else
          {
            get_release();
          }
        }
        else
        {
          get_release();
        }
      }

    })}, wait_time);

};

get_release();



