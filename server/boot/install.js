'use strict';

var installed = true; // set to the true if we have ran installed before.
module.exports = function (app) {

  if (!installed) {
    var Digger = app.models.digger;
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;


    Digger.create([
      {username: 'magazino', email: 'costamagazino@gmail.com', password: 'bloop', 'firstName': 'luis', 'lastName': 'del costa'},
    ], function (err, users) {
      if (err) throw err;


      console.log("Created User: ", users);
      //create the admin role
      Role.create({
        name: 'administrator'
      }, function (err, role) {
        if (err) throw err;

        //make bob an admin
        role.principals.create({
          principalType: RoleMapping.USER,
          principalId: users[0].id
        }, function (err, principal) {
          console.log('Created principal:', principal);

          // now it should be fine :)
        });
      });
    });
  }

};








