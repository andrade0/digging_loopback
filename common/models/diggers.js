'use strict';

module.exports = function(Diggers) {
  Diggers.settings.acls.length = 0;
  //Diggers.settings.acls = require('./diggers_acl.json');

  Diggers.register = function(email, password, cb) {

    /*Diggers.create({email: email, password: password}, function(err, userInstance) {
      cb(userInstance);
    });*/
  };

  Diggers.remoteMethod('register', {
    accepts: [{arg: 'email', type: 'string'}, {arg: 'password', type: 'string'}],
    returns: {arg: 'user', type: 'object'}
  });

  Diggers.setsettings = function(setting, value, userId, cb) {
    Diggers.find({where: {id: userId}} , (error, user) => {
      if(user)
      {
        user.settings[setting] = value;
        Cache.update({id: userId}, user, function(){
          cb(null, ['success']);
        });
      }
      else
      {
        cb(null, ['user not found']);
      }
    });
  };

  Diggers.remoteMethod('setsettings', {
    accepts: [{arg: 'setting', type: 'string'}, {arg: 'value', type: 'any'}, {arg: 'userId', type: 'string'}],
    returns: {arg: 'result', type: 'any'}
  });

  Diggers.addlike = function(track, userId, cb) {
    Diggers.find({where: {id: userId}} , (error, user) => {
      if(user)
      {
        user.likes[track.id] = track;
        Cache.update({id: userId}, user, function(){
          cb(null, ['success']);
        });
      }
      else
      {
        cb(null, ['user not found']);
      }
    });
  };

  Diggers.remoteMethod('addlike', {
    accepts: [{arg: 'track', type: 'any'}, {arg: 'userId', type: 'string'}],
    returns: {arg: 'result', type: 'any'}
  });

  Diggers.removelike = function(track, userId, cb) {
    Diggers.find({where: {id: userId}} , (error, user) => {
      if(user)
      {
        var tracks = _.where(user.likes, {old_id: track.id});
        delete user.likes[tracks[0].id];
        Cache.update({id: userId}, user, function(){
          cb(null, ['success']);
        });
      }
      else
      {
        cb(null, ['user not found']);
      }
    });
  };

  Diggers.remoteMethod('removelike', {
    accepts: [{arg: 'track', type: 'any'}, {arg: 'userId', type: 'string'}],
    returns: {arg: 'result', type: 'any'}
  });

};
