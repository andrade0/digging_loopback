const _ = require('underscore');
var digging_curl = require('../digging/curl.js');
var digging = require('../digging/digging.js');
var digging_sources = require('../digging/sources.js');
const md5 = require( 'md5' );
const moment = require( 'moment' );
const async = require( 'async' );
const S = require( 'string' );
const Promise = require('promise');
const digging_queue = require( '../digging/kue.js' );
const Fuse = require( 'fuse.js' );
const cheerio = require( 'cheerio' );

const elasticsearch = require('elasticsearch');
const ElastiClient = new elasticsearch.Client({
  host: 'http://104.197.118.37/elasticsearch',
  httpAuth: 'user:P4d61bXKw6fC'
});

module.exports = function(Cache) {

  Cache.curl = function(url, wait, check_release, cb) {
    digging_curl(Cache, url, wait, check_release, cb);
  };

  Cache.remoteMethod('curl', {
    accepts: [{arg: 'url', type: 'string'}, {arg: 'wait', type: 'string'}, {arg: 'check_release', type: 'string'}],
    returns: {arg: 'data', type: 'string'}
  });



  Cache.scrape = function(source, action, query, cb) {

    var key = md5(source+action+query);

    if(action == "get_news")
    {
      var mongo_query = {where: {key: 'dkfdsfjdsfjsdjfljldsjfldsjlfjldskjfljdslkj'}};
    }
    else
    {
      var mongo_query = {where: {key: key}};
    }

    Cache.find({where: {key: key}} , function(error, result) {

      if(result && result.length > 0)
      {
        if(result[0].expire)
        {
          var now = moment();
          var expire = moment(result[0].expire);

          if(now > expire)// expired
          {
            var process = true;
          }
          else
          {
            var process = false;
          }
        }
        else
        {
          var process = false;
        }
      }
      else
      {
        var process = true;
      }

      if (process === false)
      {
        cb(null, result[0].data);
      }
      else
      {
        var releases = [];

        /*ElastiClient.search({
          index: 'digging',
          type: 'release',
          body: {
            "query": {
              "multi_match" : {
                "query" : query,
                "fields": ["query"],
                "fuzziness": 1
              }
            },
            "from": 0,
            "size": 500
          }
        },function (error, response, status) {

          if(!error && response.hits.total > 0)
          {
            response.hits.hits.forEach(function(hit){
              releases.push(hit._source);
            });
          }
        });*/


        digging_sources(function(error, sources){

          switch(source)
          {
            case "all":
              async.each(sources, function(_source, callabck_each){

                var search_func_name1 = action;
                var search_func_name2 = action+"_1";
                var functions = [];
                if(_source[search_func_name1])
                {
                  functions.push(search_func_name1);
                }
                if(_source[search_func_name2])
                {
                  functions.push(search_func_name2);
                }

                if(functions.length == 1)
                {
                  _source[action](Cache, query, function(_releases){

                    _releases.forEach(function(r){
                      digging_queue.create('insertRelease', r).save();
                      releases.push(r);
                    });
                    callabck_each(null);
                  });
                }
                else if(functions.length == 2)
                {
                  var search_func_name1 = functions[0];
                  var search_func_name2 = functions[1];

                  _source[search_func_name1](Cache, query, null, function(pages){

                    if(pages.length > 0)
                    {
                      async.eachSeries(pages, function (page, eachPagesCallback) {
                        _source[search_func_name2](Cache, query, page, function (release) {
                          if (release) {
                            digging_queue.create('insertRelease', release).save();
                            releases.push(release);
                          }
                          eachPagesCallback(null);
                        });
                      }, function () {
                        callabck_each(null);
                      });
                    }
                    else
                    {
                      callabck_each(null);
                    }
                  });
                }
                else
                {
                  callabck_each(null);
                }

              }, function(){

                var expire = moment().add(7, 'days').format();
                if(result[0] && result[0].id)
                {
                  Cache.update({id: result[0].id}, {key: key, data: releases, expire: expire}, function(){
                    cb(null, releases);
                  });
                }
                else
                {
                  Cache.create({key: key, data: releases, expire: expire}, function(){
                    cb(null, releases);
                  });
                }

              });
              break;
            default:



              var source_object = _.findWhere(sources, {value: source});

              if(source_object)
              {
                var search_func_name1 = action;
                var search_func_name2 = action+"_1";
                var functions = [];
                if(source_object[search_func_name1])
                {
                  functions.push(search_func_name1);
                }
                if(source_object[search_func_name2])
                {
                  functions.push(search_func_name2);
                }

                if(functions.length == 1)
                {
                  source_object[action](Cache, query, function(_releases){
                    _releases.forEach(function(r){

                      digging_queue.create('insertRelease', r).save();
                      releases.push(r);

                    });
                    var expire = moment().add(7, 'days').format();

                    if(result[0] && result[0].id)
                    {
                      Cache.update({id: result[0].id}, {key: key, data: releases, expire: expire}, function(){
                        cb(null, releases);
                      });
                    }
                    else
                    {
                      Cache.create({key: key, data: releases, expire: expire}, function(){
                        cb(null, releases);
                      });
                    }
                  });
                }
                else if(functions.length == 2)
                {
                  var search_func_name1 = functions[0];
                  var search_func_name2 = functions[1];

                  source_object[search_func_name1](Cache, query, null, function (pages) {

                    if(pages.length > 0)
                    {
                      async.eachSeries(pages, function (page, eachPagesCallback) {

                        source_object[search_func_name2](Cache, query, page, function (release) {
                          if (release) {
                            digging_queue.create('insertRelease', release).save();
                            releases.push(release);
                          }
                          eachPagesCallback(null);
                        });
                      }, function () {

                        var expire = moment().add(7, 'days').format();
                        if(result[0] && result[0].id)
                        {
                          Cache.update({id: result[0].id}, {key: key, data: releases, expire: expire}, function(){
                            cb(null, releases);
                          });
                        }
                        else
                        {
                          Cache.create({key: key, data: releases, expire: expire}, function(){
                            cb(null, releases);
                          });
                        }
                      });
                    }
                    else
                    {
                      var expire = moment().add(7, 'days').format();
                      if(result[0] && result[0].id)
                      {
                        Cache.update({id: result[0].id}, {key: key, data: releases, expire: expire}, function(){
                          cb(null, releases);
                        });
                      }
                      else
                      {
                        Cache.create({key: key, data: releases, expire: expire}, function(){
                          cb(null, releases);
                        });
                      }
                    }
                  });
                }
                else
                {
                  cb(null, 'Error: action "'+action+'" not set in Source "'+source+'" not found.')
                }
              }
              else
              {
                cb(null, 'Error: Source "'+source+'" not found.')
              }
              break;
          }
        });

      }
    });
  };

  Cache.remoteMethod('scrape', {
    accepts: [{arg: 'source', type: 'string'}, {arg: 'action', type: 'string'}, {arg: 'query', type: 'string'}],
    returns: {arg: 'releases', type: 'string'}
  });

  Cache.sources = function(cb) {
    digging_sources(cb);
  };

  Cache.remoteMethod('sources', {
    accepts: [],
    returns: {arg: 'sources', type: 'string'}
  });

  Cache.candidates = function(query, cb) {

    var key = md5(query);

    Cache.find({where: {key: key}} , function(error, result) {

      if(result && result.length > 0)
      {
        if(result[0].expire)
        {
          var now = moment();
          var expire = moment(result[0].expire);

          if(expire < now)// expired
          {
            var process = true;
          }
          else
          {
            var process = false;
          }
        }
        else
        {
          var process = false;
        }
      }
      else
      {
        var process = true;
      }

      if (process === false)
      {
        cb(null, result[0].data);
      }
      else {
        digging.define_search_type(Cache, query, function(candidates){
          var expire = moment().add(7, 'days').format();
          Cache.create({key: key, data: candidates, expire: expire}, function(){
            cb(null, candidates);
          });
        });
      }
    });
  };

  Cache.remoteMethod('candidates', {
    accepts: {arg: 'query', type: 'string'},
    returns: {arg: 'candidates', type: 'string'}
  });

  Cache.suggestions = function(artist, title, cb) {

    var key = md5(artist+title);

    Cache.find({where: {key: key}} , function(error, result) {

      if(result && result.length > 0)
      {
        if(result[0].expire)
        {
          var now = moment();
          var expire = moment(result[0].expire);

          if(expire < now)// expired
          {
            var process = true;
          }
          else
          {
            var process = false;
          }
        }
        else
        {
          var process = false;
        }
      }
      else
      {
        var process = true;
      }

      if (process === false)
      {
        cb(null, result[0].data);
      }
      else {
        var discogs = require('../digging/sources/discogs.js');
        discogs.get_suggestion(Cache, {artist: artist, title: title}, function(releases){
          var expire = moment().add(7, 'days').format();
          Cache.create({key: key, data: releases, expire: expire}, function(){
            cb(null, releases);
          });
        });
      }
    });
  };

  Cache.remoteMethod('suggestions', {
    accepts: [{arg: 'artist', type: 'string'}, {arg: 'title', type: 'string'}],
    returns: {arg: 'releases', type: 'string'}
  });

  Cache.youtube = function(q, digging_youtube_search_callback) {
    if(q && q.length && q.length > 0)
    {
      q = S(q).replaceAll('(', '').s;
      q = S(q).replaceAll(')', '').s;
      q = S(q).humanize('}', '').s;
      q = q.replace(/[^\w\s]/gi, '');

      var host = 'https://www.googleapis.com';
      var path = '/youtube/v3/search?part=id%2Csnippet&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&q=';
      var url = host+path+q;
      digging_curl(Cache, url, false, false, function(err, res){
        if(res.html)
        {
          var all_results = [];
          var youtube_data = JSON.parse(res.html);

          if(youtube_data && youtube_data.items && youtube_data.items.length > 0 && youtube_data.items[0])
          {

            youtube_data.items.forEach(function(item){

              if(item.id.kind == 'youtube#video')
              {
                var the_title = item.snippet.title;

                //if(digging_contains_all_words(q, the_title))
                //{
                all_results.push({title: the_title, id: item.id.videoId});
                //}
              }
            });

            if(all_results.length > 0)
            {
              var options = {
                /*threshold: 0.4,*/
                keys: ['title','id']
              };

              var fuse = new Fuse(all_results, options);
              var fuse_result = fuse.search(q);

              if(fuse_result.length > 0)
              {
                var fuse_url_best_match = fuse_result[0];

                digging_youtube_search_callback(null, fuse_url_best_match);
              }
              else
              {
                digging_youtube_search_callback(null, null);
              }
            }
            else
            {
              digging_youtube_search_callback(null, null);
            }
          }
          else
          {
            digging_youtube_search_callback(null, null);
          }
        }
        else
        {
          digging_youtube_search_callback(null, null);
        }
      });
    }
    else
    {
      digging_youtube_search_callback(null, null);
    }
  };

  Cache.remoteMethod('youtube', {
    accepts: {arg: 'query', type: 'string'},
    returns: {arg: 'youtubeId', type: 'string'}
  });


  Cache.samples = function(urls, digging_samples_callback) {

    var samples = [];

    if(!urls || (urls && !Array.isArray(urls)) || (urls && Array.isArray(urls) && urls.length < 1))
    {
      var urls = [];
      for (i = 0; i <30; i++) {
        var id = Math.floor(Math.random() * (408737 - 10 + 1)) + 10;
        var url = 'http://www.whosampled.com/sample/'+id+'/xxxxx/';
        urls.push(url);
      }
    }

    async.each(urls, function(url, cb){
      digging_curl(Cache, url, false, false, function(error, result){

        var $ = cheerio.load(result.html);

        var sampler_artist = $('div#sampleWrap_dest div.sampleTrackArtists a').text();
        //var sampled_artist = $('div#sampleWrap_source div.sampleTrackArtists a').text();
        var sampler_track_title = $('div#sampleWrap_dest a.trackName span').text();
        //var sampled_track_title = $('div#sampleWrap_source a.trackName span').text();

        if($('section.sample-container div.sampleEntryBox'))
        {
          var sample_found = false;
          $('section.sample-container div.sampleEntryBox').each(function(k, v){
            if(k == 1)
            {
              sample_found = true;
              var youtube_url = $(this).find('iframe').attr('src');
              var appearAt = $(this).find('div.sampleTimings.sample-layout-row div.sampleTimingRight.sample-layout-row-right strong').text();

              var artist = $(this).find('div.sampleTrackInfo div.sampleTrackArtists').text().trim();
              var title = $(this).find('div.sampleTrackInfo a.trackName').text().trim();
              var year = $(this).find('div.sampleReleaseDetails div.trackLabel p span').last().text().trim();


              appearAt  = appearAt.trim().replace(" ", "").replace(" ", "").split(",");
              if(appearAt && appearAt[0] && appearAt[0].length > 3)
              {
                var tab = appearAt[0].split(':');
                //var start = (parseInt(tab[0])*60)+(parseInt(tab[0]));
                var start = tab[0];
              }
              else
              {
                var start = 0;
              }

              start = appearAt;

              //var start_str = (start/60)+':'+start%60;

              //start = start_str;

              if(youtube_url)
              {
                var sp = youtube_url.split('?');

                if(youtube_url.split)
                {
                  var sp2 = sp[0].split('/');

                  if(sp2 && sp2.length && sp2[sp2.length-1])
                  {
                    var youtube_id = sp2[sp2.length-1];

                    if(youtube_id)
                    {
                      samples.push({url: url, artist: artist, title: title, genre: sampler_artist+' - '+sampler_track_title, sampler: sampler_artist, year: year, start: start, youtubeId: youtube_id});
                      cb(null);
                    }
                    else
                    {
                      cb(null);
                    }
                  }
                  else
                  {
                    cb(null);
                  }
                }
                else
                {
                  cb(null);
                }
              }
              else
              {
                cb(null);
              }
            }
          });

          if(sample_found === false)
          {
            cb(null);
          }

        }
        else
        {
          cb(null);
        }
      });
    }, function(error){
      digging_samples_callback(error, samples);
    });

  };

  Cache.remoteMethod('samples', {
    accepts: {arg: 'urls', type: 'array'},
    returns: {arg: 'samples', type: 'array'}
  });

  Cache.elastic = function(action, query, fromm, size, digging_elastic_callback) {

    switch(action){
      case 'count':
        ElastiClient.count(function (error, response, status) {
          digging_elastic_callback(error, [response.count]);
        });
        break;
      case 'search_artist':
        ElastiClient.count({
          index: 'digging',
          type: 'release',
          body: {
            "query": {
              "match_phrase": {
                "artist": query
              }
            }
          }
        }, function (error, response) {
          var count = response.count;

          ElastiClient.search({
            index: 'digging',
            type: 'release',
            from: fromm,
            size: size,
            body: {
              "query": {
                "match_phrase": {
                  "artist": query
                }
              }
            }
          },(error, response, status) => {
            if (error || (response && response.hits.total == 0) )
            {
              digging_elastic_callback(error, []);
            }
            else
            {
              if(response && response.hits && response.hits.hits && response.hits.hits.length > 0)
              {
                digging_elastic_callback(error, [count, response.hits.hits]);
              }
              else
              {
                digging_elastic_callback(error, []);
              }
            }
          });
        });
        break;
      case 'search_label':
        ElastiClient.count({
          index: 'digging',
          type: 'release',
          body: {
            "query": {
              "match_phrase": {
                "label": query
              }
            }
          }
        }, function (error, response) {
          var count = response.count;

          ElastiClient.search({
            index: 'digging',
            type: 'release',
            from: fromm,
            size: size,
            body: {
              "query": {
                "match_phrase": {
                  "label": query
                }
              }
            }
          },(error, response, status) => {
            if (error || (response && response.hits.total == 0) )
            {
              digging_elastic_callback(error, []);
            }
            else
            {
              if(response && response.hits && response.hits.hits && response.hits.hits.length > 0)
              {
                digging_elastic_callback(error, [count, response.hits.hits]);
              }
              else
              {
                digging_elastic_callback(error, []);
              }
            }
          });
        });
        break;
      case 'search_title':
        ElastiClient.count({
          index: 'digging',
          type: 'release',
          body: {
            "query": {
              "match_phrase": {
                "title": query
              }
            }
          }
        }, function (error, response) {
          var count = response.count;

          ElastiClient.search({
            index: 'digging',
            type: 'release',
            from: fromm,
            size: size,
            body: {
              "query": {
                "match_phrase": {
                  "title": query
                }
              }
            }
          },(error, response, status) => {
            if (error || (response && response.hits.total == 0) )
            {
              digging_elastic_callback(error, []);
            }
            else
            {
              if(response && response.hits && response.hits.hits && response.hits.hits.length > 0)
              {
                digging_elastic_callback(error, [count, response.hits.hits]);
              }
              else
              {
                digging_elastic_callback(error, []);
              }
            }
          });
        });
        break;
      case 'search':
        ElastiClient.count({
          index: 'digging',
          type: 'release',
          body: {
            "query": {
              "match_phrase": {
                "query": query
              }
            }
          }
        }, function (error, response) {
          var count = response.count;

          ElastiClient.search({
            index: 'digging',
            type: 'release',
            from: fromm,
            size: size,
            body: {
              "query": {
                "match_phrase": {
                  "query": query
                }
              }
            }
          },(error, response, status) => {
            if (error || (response && response.hits.total == 0) )
            {
              digging_elastic_callback(error, []);
            }
            else
            {
              if(response && response.hits && response.hits.hits && response.hits.hits.length > 0)
              {
                digging_elastic_callback(error, [count, response.hits.hits]);
              }
              else
              {
                digging_elastic_callback(error, []);
              }
            }
          });
        });
        break;
      default:
        ElastiClient.count({
          index: 'digging',
          type: 'release'
        }, function (error, response) {
          var count = response.count;

          ElastiClient.search({
            index: 'digging',
            type: 'release',
            from: fromm,
            size: size
          },(error, response, status) => {
            if (error || (response && response.hits.total == 0) )
            {
              digging_elastic_callback(error, []);
            }
            else
            {
              if(response && response.hits && response.hits.hits && response.hits.hits.length > 0)
              {
                digging_elastic_callback(error, [count, response.hits.hits]);
              }
              else
              {
                digging_elastic_callback(error, []);
              }
            }
          });
        });
        break;
    }

  };

  Cache.remoteMethod('elastic', {
    accepts: [{arg: 'action', type: 'string'}, {arg: 'query', type: 'string'}, {arg: 'fromm', type: 'string'}, {arg: 'size', type: 'string'}],
    returns: {arg: 'result', type: 'array'}
  });
};

