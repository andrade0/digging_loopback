'use strict';
var kue = require('kue');
const elasticsearch = require('elasticsearch');
const ElastiClient = new elasticsearch.Client({
  host: 'http://104.197.118.37/elasticsearch',
  httpAuth: 'user:P4d61bXKw6fC'
});

const digging_queue = kue.createQueue();

digging_queue.process('insertRelease', function(job, done) {

  setTimeout( function(){

    var release = job.data;

    if(release.id)
    {
      ElastiClient.search({
        index: 'digging',
        type: 'release',
        body: {
          "query": {
            "terms": {
              "_id": [ release.id ]
            }
          }
        }
      },function (error, response, status) {

        if(!error && response.hits.total == 0)
        {
          ElastiClient.index({
            index: 'digging',
            type: 'release',
            id: release.id,
            body: release
          }, function (error, response) {
            done(null, release);
          });
        }
        else
        {
          job.log('Release already there');
          done(null, 'Release already there');
        }
      });
    }
    else
    {
      job.log('Bad release structure');
      done(null, 'Bad release structure');
    }

  }, 1000 );
});

module.exports = digging_queue;