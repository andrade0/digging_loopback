const _ = require('lodash');
const path = require('path');
const electron = require('electron');
const app = electron.app || electron.remote.app;
const userData = app.getPath('userData'); ///Users/andrade/Library/Application\ Support/diggin-music
const async = require('async');
const fs = require('fs');
const exists = require('exists-file');
const rimraf = require('rimraf');
const md5 = require( 'md5' );

digging_storage = {
    clear: function(callback) {
        var $this = this;
        const userData = userData;
        const jsonFiles = path.join(userData, '*.json');
        rimraf(jsonFiles, callback);
    },
    remove: function(key, callback) {
        var $this = this;
        async.waterfall([
            async.asyncify(_.partial($this.getFileName, key)),
            rimraf
        ], callback);
    },
    set : function(key, json, callback) {

        var $this = this;
        async.waterfall([
            async.asyncify(_.partial($this.getFileName, key)),
            function(fileName, callback) {

                const data = JSON.stringify(json);

                if (!data) {
                    return callback(new Error('Invalid JSON data'));
                }

                fs.writeFile(fileName, data, callback);
            }
        ], callback);
    },
    get : function(key, callback) {

        var $this = this;
        async.waterfall([
            async.asyncify(_.partial($this.getFileName, key)),
            function(fileName, callback) {

                fs.readFile(fileName, {
                    encoding: 'utf8'
                }, function(error, object) {

                    if (!error) {
                        return callback(null, object);
                    }

                    if (error.code === 'ENOENT') {
                        return callback(null, false);
                    }

                    return callback(error);
                });
            },
            function(object, callback) {
                var objectJSON = {};
                try {
                    objectJSON = JSON.parse(object);
                } catch (error) {
                    return callback(new Error('Invalid data'));
                }

                return callback(null, objectJSON);
            }
        ], callback);
    },
    getFileName : function(key) {
        var $this = this;
        if (!key) {
            throw new Error('Missing key');
        }

        if (!_.isString(key) || key.trim().length === 0) {
            throw new Error('Invalid key');
        }

        const keyFileName = path.basename(key, '.json') + '.json';

        const escapedFileName = encodeURIComponent(keyFileName);

        return path.join(userData, escapedFileName);
    },
    remove: function(key, callback) {
        var $this = this;
        async.waterfall([
            async.asyncify(_.partial($this.getFileName, key)),
            rimraf
        ], callback);
    },
    updateUserInStore: function(user, cb) {
        var $this = this;
        $this.user = user;
        $this.set('user', user, cb);
    },
    removeUserInStore: function() {
        var $this = this;
        $this.remove('user', function(){

        });
    },
    cacheUrlSet: function(url, data, cb){
        var $this = this;
        var key = md5(url);
        $this.set(key, data, cb);
    },
    cacheUrlGet: function(url, cb){
        var $this = this;
        var key = md5(url);
        $this.get(key, cb);
    },
    logUser: function(callback){
        var $this = this;
        $this.get('user', function(error, user)
        {
            if (error || !user) {
                callback(null);
            }
            else
            {
                //user is in device data
                //update it
                $this.getUserFromDrupal(user.credential.email, user.credential.password, function(response){
                    if(response && response.status == true)
                    {
                        callback(response.user);
                    }
                    else
                    {
                        callback(null);
                    }
                });
            }
        });
    },
    getUser: function(callback){
        var $this = this;

        $this.get('user', function(error, user)
        {
            if (error || !user) {
                callback(null);
            }
            else
            {
                callback(user);
            }
        });

        /*else
        {
            $this.get('user', function(error, user)
            {
                if(error || !user)
                {
                    callback(null);
                }
                else
                {
                    $this.getUserFromDrupal(user.credential.email, user.credential.password, function(response){
                        if(response && response.status == true)
                        {
                            $this.set('user', response.user, function(error, result){
                                if(error)
                                {
                                    callback(null);
                                }
                                else
                                {
                                    $this.user = response.user;
                                    callback(response.user);
                                }
                            });
                        }
                        else
                        {
                            callback(null);
                        }
                    });
                }
            });
        }*/
    },
    saveUserInDrupal: function(callback){
        var $this = this;

        if($this.getUser)
        {
            $this.getUser(function(user){
                var client = new Client();

                var args = {
                    data: {email: user.credential.email, password: user.credential.password, data: {sources: user.sources, playlists: user.playlists, likes: user.likes}}
                };

                client.post("http://digging-music.io/digging-update-data", args, function (data, response) {
                    data = JSON.parse(data.toString('utf8'));
                    $this.set('user', user, function(){
                        callback(true);
                    });
                });
            });
        }
    },
    saveUserInMemory: function(user, callback){
        var $this = this;
        $this.set('user', user, function(error, result){
            callback();
        });
    },
    getUserFromDrupal: function(email, password, callback){
        var $this = this;
        var client = new Client();
        var args = {
            data: {email: email, password: password}
        };

        client.post("http://digging-music.io/digging-login", args, function (data, response) {
            data = JSON.parse(data.toString('utf8'));
            if(data.status == 'success')
            {
                var type = data.user.field_subscription_type.und[0].value;
                var date = data.user.field_premium_expire_date.und[0].value;
                var name = data.user.field_name.und[0].value;
                var last_name = data.user.field_last_name.und[0].value;

                if(type == 0)
                {
                    var subscription_type = 'Freemium';
                }
                else
                {
                    var subscription_type = 'Premium';
                }

                var digging_data = data.user.data;

                var user = {
                    uid: data.user.uid,
                    mail: data.user.mail,
                    credential: args.data,
                    type: type,
                    subscription_type: subscription_type,
                    subscription_expire: date,
                    name: name,
                    last_name: last_name
                };

                if(digging_data["sources"])
                {
                    user.sources = digging_data.sources;
                }
                else
                {
                    user.sources = [];
                }

                if(digging_data["likes"])
                {
                    user.likes = digging_data.likes;
                }
                else
                {
                    user.likes = [];
                }

                if(digging_data["playlists"])
                {
                    user.playlists = digging_data.playlists;
                }
                else
                {
                    user.playlists = [];
                }

                $this.updateUserInStore(user, function(e,r){
                    if(!e)
                    {
                        callback({status: true, user: user});
                    }
                    else
                    {
                        callback({status: false, msg: 'A technical probleme occured, please contact administrator.'});
                    }
                });
            }
            else
            {
                callback({status: false, msg: data.msg});
            }
        });
    },
    flush: function(callback){
        const userData = app.getPath('userData');
        const jsonFiles = path.join(userData, '*.json');
        rimraf(jsonFiles, callback);
    }
};







