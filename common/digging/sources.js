'use strict';
var fs = require('fs');
var path = require('path');
const async = require( 'async' );
const sources_path = __dirname+'/sources';

const digging_sources = function(callback){
  var sources = [];
  async.each(fs.readdirSync(sources_path), function(name, cb_each){
    var s = require(path.join(__dirname.replace('modules/sources',''), 'sources')+'/'+name);
    if(s.name)
    {
      sources.push(s);
      cb_each(null);
    }
    else
    {
      cb_each(null);
    }
  }, function(error, result){
    callback(null, sources);
  });
};

module.exports = digging_sources;
