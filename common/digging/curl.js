'use strict';

var request = require('request');
var async = require('async');
var moment = require('moment');
const md5 = require( 'md5' );
const elasticsearch = require('elasticsearch');
const ElastiClient = new elasticsearch.Client({
  host: 'http://104.197.118.37/elasticsearch',
  httpAuth: 'user:P4d61bXKw6fC'
});

let process_curl = function(Cache, wait, check_release, url, cb){

  let key = md5(url);

  var sleeps = [];
  sleeps.push(900);
  sleeps.push(1800);
  sleeps.push(1900);
  sleeps.push(1200);
  sleeps.push(1600);
  sleeps.push(3600);
  sleeps.push(2900);
  sleeps.push(950);
  sleeps.push(1550);
  sleeps.push(2100);
  sleeps.push(1700);
  sleeps.push(1500);
  sleeps.push(1700);

  var user_agents = [];
  user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2");
  user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
  user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.0; it-IT; rv:1.7.12) Gecko/20050915");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.12 Safari/537.36 OPR/14.0.1116.4");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36 OPR/15.0.1147.24 (Edition Next)");
  user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
  user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
  user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
  user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
  user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
  user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");

  var random_index = Math.floor((Math.random() * 10));
  var user_agent = user_agents[random_index];

  if(wait)
  {
    var wait_time = user_agents[random_index];
  }
  else
  {
    var wait_time = 10;
  }

  setTimeout(function(){
    request(url, {headers: {
      "User-Agent": user_agent,
      "timeout": 2000
    }}, function (error, response, html)
    {
      if(error)
      {
        var curl_result = {html: false, url: key};
      }
      else
      {
        var curl_result = {html: html, url: key};
      }


      if(check_release)
      {

        Cache.update({key: key}, {key: key, data: curl_result}, function(e, r){
          if(!e && r && r.count && r.count >= 1)
          {
            cb(null, curl_result);
          }
          else
          {
            Cache.create({key: key, data: curl_result}, function(){
              cb(null, curl_result);
            });
          }
        });
      }
      else
      {
        var expire = moment().add(7, 'days').format();

        Cache.update({key: key}, {key: key, data: curl_result, expire: expire}, function(e, r){
          if(!e && r && r.count && r.count >= 1)
          {
            cb(null, curl_result);
          }
          else
          {
            Cache.create({key: key, data: curl_result, expire: expire}, function(){
              cb(null, curl_result);
            });
          }
        });
      }

    });
  }, wait_time);
};

let digging_curl = function(Cache, url, wait, check_release, cb){

  let key = md5(url);

  if(!wait || (wait && wait !== true))
  {
    wait = false;
  }

  if(!check_release || (check_release && check_release !== true))
  {
    check_release = false;
  }



  Cache.find({where: {key: key}} , function(error, result){

    if(result && result.length > 0)
    {
      if(result[0].expire)
      {
        var now = moment();
        var expire = moment(result[0].expire);

        if(expire < now)// expired
        {
          var process = true;
        }
        else
        {
          var process = false;
        }
      }
      else
      {
        var process = false;
      }
    }
    else
    {
      var process = true;
    }

    if(process === false)
    {
      cb(null, result[0].data);
    }
    else
    {
      if(check_release)
      {
        ElastiClient.search({
          index: 'digging',
          type: 'release',
          body: {
            "query": {
              "match_phrase": {
                "url": url
              }
            }
          }
        },function (error, response, status) {

          if (error || (response && response.hits.total == 0) )
          {
            process_curl(Cache, wait, check_release, url, cb);
          }
          else
          {
            var release = response.hits.hits[0];
            release._id = response.hits.hits[0]._id;
            cb(null, {release: release._source, url: url});
          }
        });
      }
      else
      {
        process_curl(Cache, wait, check_release, url, cb);
      }

    }

  });
};


module.exports = digging_curl;
