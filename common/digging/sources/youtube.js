'use strict';
const digging = require('../digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );
var digging_curl = require('../curl.js');
var ypi = require('youtube-channel-videos');


module.exports = {
  name: 'Youtube',
  value: 'youtube',
  //Return sting image uri
  logo: 'public/img/sources/youtube.png',

  get_news: function(Cache, channels, get_news_callback){

    var channels = ['UCUdIW-0oPT33y_OjE_ZnQag', 'UCfHJRUpF8FlM4FQ0zUtZIMA', 'UCqSMQXJ8MieGFIxgZNDeLdQ', ''];

    var $this = this;

    var all_items = [];
    async.each(channels, (channelId, cb)=>{
      ypi.channelVideos("AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc", "UCUdIW-0oPT33y_OjE_ZnQag", function(channelItems) {

        channelItems.forEach(function(item){

          var release = {};
          release.id = md5(item.id.videoId);
          var title = item.snippet.title;
          var expl = title.split(' - ');
          if(expl.length == 2)
          {
            release.title = expl[1];
            release.artist = expl[0];
          }
          else
          {
            release.title = title;
            release.artist = null;
          }
          release.label = item.snippet.channelTitle;
          release.source = 'youtube';
          release.genre = "house";
          release.played_by = null;
          release.logo = $this.logo;
          release.cover = 'public/img/vinyl-digging-transparent.png';
          release.kind = 'release';
          release.year = moment(item.snippet.publishedAt).year();
          release.release_date = moment(item.snippet.publishedAt).unix();
          release.tracks = [
            {
              id: md5(item.id.videoId+"1"),
              artist: release.artist,
              title: release.title,
              logo: $this.logo,
              label: item.snippet.channelTitle,
              cover: 'public/img/vinyl-digging-transparent.png',
              youtube: item.id.videoId,
              item_type: 'track',
              release_title: release.title,
              genre: 'house',
              year: moment(item.snippet.publishedAt).year(),
              release_date: moment(item.snippet.publishedAt).unix()
            }
          ];

          var keywords_track = [release.title, release.artist, release.label, release.genre, release.played_by, release.kind, release.source];
          release.query = keywords_track.join(' ');

          var alias = S(release.artist+'-'+release.title).latinise().s;
          alias = S(alias).strip("'", '"', '!','`','*','£','ù','%','$','¨','°','(',')','§','[',']','{','}',',').s;
          alias = S(alias).humanize().s;
          alias = alias.toLowerCase();
          alias = S(alias).replaceAll(' ', '-').s;
          alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;
          release.alias = alias;
          release.url = 'https://www.youtube.com/watch?v='+item.id.videoId;

          all_items.push(release);
        });
        cb(null);
      });
    }, function(e, r){
      get_news_callback(all_items);
    });
  }
};
