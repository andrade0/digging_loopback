const digging = require('./digging.js');
const _ = require('underscore');
const async = require( 'async' );
const S = require( 'string' );
const fs = require( 'fs' );
const cheerio = require( 'cheerio' );
const md5 = require( 'md5' );
const moment = require( 'moment' );
const Fuse = require( 'fuse.js' );

module.exports = {
    digging_mongo_url: 'mongodb://ns508027.ip-198-245-60.net:3333/digging',
    //digging_mongo_url: 'mongodb://localhost/digging',
    /*digging_mongo_url: 'mongodb://andrade0:030303cC@ds161225.mlab.com:61225/digging',*/
    user: null,
    connect: function(connect_callback){
        MongoClient.connect(this.digging_mongo_url, function(err, db)
        {
            if (err)
            {
                console.log(err);
                connect_callback(null);
            }
            else
            {
                connect_callback(db);
            }
        });
    },
    close: function(db){
        //console.log(db);
        //db.close();
    },
    release_exists: function(db, release, release_exists_callback){
        var collection = db.collection('releases');

        if(collection)
        {
            collection.find({_id: release._id}).toArray(function(err, result) {
                if(err)
                {
                    console.log(err);
                    release_exists_callback(null);
                }
                else
                {
                    if(result && result.length && result.length > 0 && result[0])
                    {
                        release_exists_callback(result[0]);
                    }
                    else
                    {
                        release_exists_callback(null);
                    }
                }
            });
        }
        else
        {
            console.log("Collection releases doesn't exists");
            release_exists_callback(null);
        }
    },
    insert_releases: function(db, releases){

        var $this = this;
        releases.forEach(function(release){
            $this.release_exists(db, release, function(r){
                if(!r)
                {
                    var collection = db.collection('releases');
                    if(collection)
                    {
                        collection.insertMany([release], function(err, result) {

                        });
                    }
                }
            })
        });
    },
    url_exists: function(db, url, url_exists_callback){
        var collection = db.collection('urls');

        if(collection)
        {
            collection.find({url: url}).toArray(function(err, result) {
                if(err)
                {

                    url_exists_callback(null);
                }
                else
                {
                    if(result && result.length && result.length > 0 && result[0])
                    {
                        url_exists_callback(true);
                    }
                    else
                    {
                        url_exists_callback(null);
                    }
                }
            });
        }
        else
        {
            console.log("Collection Urls doesn't exists");
            url_exists_callback(null);
        }
    },
    insert_url: function(db, url){
        var collection = db.collection('urls');
        if(collection)
        {
            collection.insertMany([{url: url}], function(err, result) {

            });
        }
    },
    exists: function(db, object, exists_callback){
        var $this = this;

        async.waterfall([
            function(waterfall_callback){
                $this.release_exists(db, object, function(release){
                    if(release)
                    {
                        waterfall_callback({status: true, collection: 'releases', data: release});
                    }
                    else
                    {
                        waterfall_callback(null);
                    }
                });
            },
            function(waterfall_callback){
                $this.url_exists(db, object.url, function(exists){
                    if(exists)
                    {
                        waterfall_callback({status: true, collection: 'urls'});
                    }
                    else
                    {
                        waterfall_callback({status: false});
                    }
                });
            }
        ], function(error){
            exists_callback(error);
        });
    },
    get_release: function(db, url, get_release_callback){
        get_release_callback(null);
    },
    search_artist: function(db, query, search_artist_callback){
        var query =  {$or: [ { 'artist': new RegExp(query, 'i') }, { 'tracks.artist' : new RegExp(query, 'i') } ] };
        var collection = db.collection('releases');
        collection.find(query).toArray(function(error, result){
            if(error)
            {
                search_artist_callback([]);
            }
            else
            {
                search_artist_callback(result);
            }
        });
    },
    search_label: function(db, query, search_label_callback){
        var query =  { $or: [ { 'label': new RegExp(query, 'i') }, { 'tracks.label' : new RegExp(query, 'i') } ] };
        var collection = db.collection('releases');
        collection.find(query).toArray(function(error, result){
            if(error)
            {
                search_label_callback([]);
            }
            else
            {
                search_label_callback(result);
            }
        });
    },
    search_all: function(db, query, search_all_callback){
        var query =  {query: new RegExp(query, 'i')};
        var collection = db.collection('releases');
        collection.find(query).toArray(function(error, result){
            if(error)
            {
                search_all_callback([]);
            }
            else
            {
                search_all_callback(result);
            }
        });
    },
    get_news: function(db, get_news_callback){


        var query = {};


        /*console.log($user.preferences);
        console.log(sources_list);
        console.log(genres_list);
        console.log(query);*/


        var collection = db.collection('news');
        collection.find(query).toArray(function(error, result){
            if(error)
            {
                get_news_callback([]);
            }
            else
            {
                get_news_callback(result);
            }
        });
    }
};
